/** @type {import('tailwindcss').Config} */

const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  mode: 'jit',
  theme: {
    colors: {
      primary: '#005333',
      secondary: '#b58d4e',
      terciary: '#9a191e',
      neutral: colors.stone,
      success: colors.green,
      danger: colors.red,
      warning: colors.yellow,
      slate: colors.slate,
      orange: colors.orange,
    },
    willChange: {
      'left-right': 'left, right',
    },
    extend: {
      animation: {
        'infinite-scroll': 'infinite-scroll 400s linear infinite',
        'wiggle': 'wiggle 1s ease-in-out infinite',

      },
      keyframes: {
        'infinite-scroll': {
          from: { transform: 'translateX(0)' },
          to: { transform: 'translateX(-5000rem)' },
        },
        'wiggle': {
          '0%, 100%': { transform: 'rotate(-3deg)' },
          '50%': { transform: 'rotate(3deg)' },
        }
      }

    },
    fontFamily: {
      // futuraHeavy: ['futura heavy', 'Futura'],
      // futuraBook: ['futura book', 'Futura'],
      // futuraRegular: ['futura regular', 'Futura'],
      cocosignumMaiuscolettoRegular: ['cocosignum maiuscoletto regular', 'Cocosignum'],
      cocosignumMaiuscolettoUltraLight: ['cocosignum maiuscoletto ultralight', 'Cocosignum'],
      cocosignumMaiuscolettoLight: ['cocosignum maiuscoletto light', 'Cocosignum'],
      cocosignumMaiuscolettoHeavy: ['cocosignum maiuscoletto heavy', 'Cocosignum'],
      cocosignumMaiuscolettoBold: ['cocosignum maiuscoletto bold', 'Cocosignum'],
      chapaza: ['chapaza', 'Chapaza'],
      chapazaItalic: ['chapaza italic', 'Chapaza'],
      albertSans: ['albert sans ', 'Albert Sans'],
      albertSansItalic: ['albert sans italic', 'Albert Sans'],
    }
  },
  plugins: [],
}





