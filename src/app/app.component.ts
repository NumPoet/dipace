import { Component, OnInit } from '@angular/core';
import {faInstagram, faTiktok, faYoutube, faFacebook} from '@fortawesome/free-brands-svg-icons';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('slideAnimation', [
      transition('* => *', [
        style({ transform: 'translateX(-200%)' }),
        animate('0s ease-out', style({ transform: 'translateX(0)' }))
      ])
    ])
  ]
})
export class AppComponent implements OnInit {
  title = 'DiPace';

  faInstagram = faInstagram;
  faTiktok = faTiktok;
  faYoutube = faYoutube;
  faFacebook = faFacebook;
  faXmark = faXmark;
  items: any[] = []; // Populate this array with carousel items



  images: string[] = [
    'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/01_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/02_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/03_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/04_sm.jpg',  'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/05_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/06_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/07_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/08_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/09_sm.jpg',// Add more images as needed
    // Repeat the first few images to create an infinite loop effect
    'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/01_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/02_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/03_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/04_sm.jpg',  'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/05_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/06_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/07_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/08_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/09_sm.jpg',
  ];

  imagesCertificates: String[] = [
    'https://storage.googleapis.com/dipace-assets/certificaciones/Dise%C3%B1o%20sin%20t%C3%ADtulo%20(11).png',
    'https://storage.googleapis.com/dipace-assets/certificaciones/dise%C3%B1op.png',
    'https://storage.googleapis.com/dipace-assets/certificaciones/RestaurantGuru_Certificate1.png',
    'https://storage.googleapis.com/dipace-assets/certificaciones/best%20italian.png',
    'https://storage.googleapis.com/dipace-assets/certificaciones/ccim_logo.png',

    // 'https://storage.googleapis.com/dipace-assets/certificaciones/image-20220729085738-1.jpeg',
    'https://storage.googleapis.com/dipace-assets/certificaciones/special_img.png',
    'https://storage.googleapis.com/dipace-assets/certificaciones/tripadvisor-certificate-2018-transparency.png',
    // 'https://storage.googleapis.com/dipace-assets/certificaciones/unnamed.jpg'
  ]; // Populate this array with urls photos

  imagesTimeLaps: String[] = [
    'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/01_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/02_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/03_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/04_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/05_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/06_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/07_sm_timeline_new.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/08_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/10_sm_timeline.jpg'
  ]; //

  activeMenu: boolean = false;

  togglemenu() {
    this.activeMenu = !this.activeMenu;

  }

  ngOnInit(): void {
    setInterval(() => {
      this.images.push('https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/01_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/02_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/03_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/04_sm.jpg',  'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/05_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/06_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/07_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/08_sm.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-Banner/09_sm.jpg'); // Rotate images array
    }, 0); // Change image every 3 seconds

    setInterval(() => {
      this.imagesTimeLaps.push('https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/01_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/02_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/03_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/04_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/05_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/06_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/07_sm_timeline_new.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/08_sm_timeline.jpg', 'https://storage.googleapis.com/dipace-assets/Banner-Carousel/Carrete-timeline/10_sm_timeline.jpg'); // Rotate images array
    }, 0);
  }
}
